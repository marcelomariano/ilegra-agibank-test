package br.com.ilegra.agibank.fileprocessortest.tests;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.ilegra.agibank.fileprocessortest.AgibankApplication;
import br.com.ilegra.agibank.fileprocessortest.processor.ProcessorFileBusiness;
import br.com.ilegra.agibank.fileprocessortest.utils.FileManager;

@ExtendWith(MockitoExtension.class)
class AgibankApplicationTests {

	@InjectMocks private ProcessorFileBusiness processorFileBusiness;

	@Mock private FileManager fileManager;

	@Test
	void contextLoads() {
		AgibankApplication.main(new String[] {});
		Assertions.assertTrue(true);
	}

	@Test
	void fileLoadNullPointerException() {
		NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> {
			processorFileBusiness.loadFile(StringUtils.EMPTY);
		});
		Assertions.assertNull(exception.getMessage());
		Assertions.assertEquals(null, exception.getMessage());
	}

}
