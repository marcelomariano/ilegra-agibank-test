package br.com.ilegra.agibank.fileprocessortest.model;

import java.math.BigDecimal;
import java.util.Optional;

import br.com.ilegra.agibank.fileprocessortest.constant.Messages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import lombok.experimental.Wither;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@Wither
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Seller extends Mergin {

	private static final String TYPE = "001";

	private String 		cpf;
	private String 		name;
	private BigDecimal 	salary;

	@SneakyThrows
	public Seller(String[] args) {
		setType(TYPE);
		Optional.of(isValid(args)).filter(Boolean::booleanValue).ifPresent(valid -> {
			if (!valid) {
				Messages.validationException();
			}
		});

		withCpf(args[1]).withName(args[2]).withSalary(new BigDecimal(args[3]));
	}

	protected boolean isValid(String[] args) {
		return args.length == 4 && getType().equals(args[0]);
	}

}