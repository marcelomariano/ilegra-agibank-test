package br.com.ilegra.agibank.fileprocessortest.model;

import lombok.Data;

@Data
public abstract class Mergin {
	private String type;
	protected abstract boolean isValid(String[] args);
}
