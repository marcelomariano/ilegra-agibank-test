package br.com.ilegra.agibank.fileprocessortest.processor;

import java.io.BufferedWriter;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.ilegra.agibank.fileprocessortest.model.Client;
import br.com.ilegra.agibank.fileprocessortest.model.Mergin;
import br.com.ilegra.agibank.fileprocessortest.model.Sale;
import br.com.ilegra.agibank.fileprocessortest.model.Seller;
import br.com.ilegra.agibank.fileprocessortest.utils.EncodeDecodeUtils;
import br.com.ilegra.agibank.fileprocessortest.utils.FileManager;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@Component
@RequiredArgsConstructor
public class ProcessorFileBusiness {

	private final FileManager fileManager;

	public void proccessFile(String fileName) {
		if (fileName != null && fileName.endsWith(".dat")) {
			List<Mergin> dataList = loadFile(fileName);
			reportGenerator(fileName, dataList);
		}
	}

	@SneakyThrows
	@SuppressWarnings("static-access")
	public List<Mergin> loadFile(String fileName) {
		return fileManager.fileRead(fileManager.pathToFile(fileName))
				.filter(line -> (line.startsWith("001") || line.startsWith("002") || line.startsWith("003")))
				.map(line -> EncodeDecodeUtils.decodeLine(line)).collect(Collectors.toList());
	}

	@SneakyThrows
	@SuppressWarnings("static-access")
	boolean reportGenerator(String fileName, List<Mergin> dataList) {
		BufferedWriter writer = fileManager.write(fileManager.resolveDestReport(fileName));
		writer.write("Ttoal de clientes: ".concat(Long.toString(count(dataList, Client.class))));
		writer.newLine();
		writer.write("Total de vendedores: ".concat(Long.toString(count(dataList, Seller.class))));
		writer.newLine();
		writer.write("ID da venda mais cara: ".concat(findMostExpensiveSale(dataList)));
		writer.newLine();
		writer.write("O pior vendedor: ".concat(findWorstSeller(dataList)));
		return true;
	}

	public long count(List<Mergin> dataList, Class<? extends Mergin> clazz) {
		if (clazz.isInstance(Client.class)) {
			return dataList.stream().filter(Client.class::isInstance).count();
		} else {
			return dataList.stream().filter(Seller.class::isInstance).count();
		}
	}

	public String findMostExpensiveSale(List<Mergin> dataList) {
		return Optional
				.ofNullable(dataList.stream().filter(Seller.class::isInstance).map(item -> (Sale) item)
						.max(Comparator.comparing(Sale::getTotalSale)).orElse(null))
				.map(s -> s.getSaleId()).orElse("Has no sale");
	}

	@SneakyThrows
	public String findWorstSeller(List<Mergin> dataList) {

		Map<String, BigDecimal> saleBySeller = dataList.stream().filter(item -> item instanceof Sale)
				.map(item -> (Sale) item).collect(Collectors.groupingBy(Sale::getSalesman,
						Collectors.mapping(Sale::getTotalSale, Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))));

		return Optional.ofNullable(saleBySeller).map(bnk -> {
			Mergin listOfSellers = dataList.stream()
					.filter(item -> item instanceof Seller && !bnk.keySet().contains(((Seller) item).getName()))
					.findFirst().orElse(null);

			if (Objects.nonNull(listOfSellers)) {
				return ((Seller) listOfSellers).getName();
			}
			return saleBySeller.entrySet().stream().min(Map.Entry.comparingByValue()).get().getKey();
		}).orElseThrow(() -> new RuntimeException("No data!"));

	}


}