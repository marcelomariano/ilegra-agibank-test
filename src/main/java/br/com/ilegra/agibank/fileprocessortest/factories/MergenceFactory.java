package br.com.ilegra.agibank.fileprocessortest.factories;

import br.com.ilegra.agibank.fileprocessortest.constant.Messages;
import br.com.ilegra.agibank.fileprocessortest.exceptions.BusinessException;
import br.com.ilegra.agibank.fileprocessortest.model.Client;
import br.com.ilegra.agibank.fileprocessortest.model.Mergin;
import br.com.ilegra.agibank.fileprocessortest.model.Sale;
import br.com.ilegra.agibank.fileprocessortest.model.Seller;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MergenceFactory {

	public static Mergin decode(String[] data) throws BusinessException {
		switch (data[0]) {
		case "001":
			return new Seller(data);
		case "002":
			return new Client(data);
		case "003":
			return new Sale(data);
		default:
			 throw new  BusinessException(Messages.INVALID_DATA);
		}
	}
}
