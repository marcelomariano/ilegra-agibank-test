package br.com.ilegra.agibank.fileprocessortest.utils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.ilegra.agibank.fileprocessortest.constant.RegexConstants;
import lombok.SneakyThrows;

@Component
public class FileManager {

	@Value("${sourceFolder}")
	private static String 				sourceFolder;

	private static String 				regex = RegexConstants.REGEX_FILE_NAME;

	@Value("${replaced}")
	private static String 				replaced;

	@Value("${dest}")
	private static String 				dest;

	@SneakyThrows
	public static Stream<String> fileRead(Path filePath) {
		return Files.lines(filePath);
	}

	public static Path pathToFile(String fileName) {
		return Paths.get(sourceFolder).resolve(fileName);
	}

	public static Path resolveDestReport(String fileName) {
		String fileDestinationName = fileName.replaceAll(regex, replaced);
		return Paths.get(dest).resolve(fileDestinationName);
	}

	public static BufferedWriter write(Path destinationepath) throws IOException {
		return Files.newBufferedWriter(destinationepath);
	}

}
