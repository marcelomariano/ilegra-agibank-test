package br.com.ilegra.agibank.fileprocessortest.utils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import br.com.ilegra.agibank.fileprocessortest.constant.RegexConstants;
import br.com.ilegra.agibank.fileprocessortest.factories.MergenceFactory;
import br.com.ilegra.agibank.fileprocessortest.model.Item;
import br.com.ilegra.agibank.fileprocessortest.model.Mergin;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EncodeDecodeUtils {

	public static String[] decode(String item) {
		return item.replaceAll(RegexConstants.DECODE, StringUtils.EMPTY).split("-");
	}

	public static List<Item> decodeList(String itemsList, BigDecimal totalSale) {
		return Arrays.asList(itemsList.split(",")).stream().map(item -> resolveItem(item, totalSale)).collect(Collectors.toList());
	}

	@SneakyThrows
	private static Item resolveItem(String itemData, BigDecimal totalSale) {
		Item saleItem = new Item(itemData);
		totalSale = (totalSale.add(saleItem.getTotalItem()));
		return saleItem;
	}
	
	
	
	@SneakyThrows
	public static Mergin decodeLine(String line) {
		return MergenceFactory.decode(line.split(RegexConstants.SPLITER));
	}
}