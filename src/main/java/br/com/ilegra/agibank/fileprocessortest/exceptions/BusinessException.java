package br.com.ilegra.agibank.fileprocessortest.exceptions;

public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4765150731207962690L;

	public BusinessException(String message) {
		super(message);
	}
}
