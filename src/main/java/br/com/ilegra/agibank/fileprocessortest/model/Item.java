package br.com.ilegra.agibank.fileprocessortest.model;

import java.math.BigDecimal;
import java.util.Optional;

import br.com.ilegra.agibank.fileprocessortest.constant.Messages;
import br.com.ilegra.agibank.fileprocessortest.exceptions.BusinessException;
import br.com.ilegra.agibank.fileprocessortest.utils.EncodeDecodeUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Wither;
import lombok.extern.log4j.Log4j2;

@Data
@Log4j2
@Wither
@Builder
@AllArgsConstructor
public class Item {

	private String 		itemId;
	private int 		quantity;
	private BigDecimal 	priceItem;
	private BigDecimal 	totalItem;

	public Item(String item) throws BusinessException {
		String[] items = EncodeDecodeUtils.decode(item);
		Optional.of(isValid(items)).filter(Boolean::booleanValue).ifPresent(valid -> {
			if (!valid) {
				Messages.validationException();
			}
		});
		withItemId(items[0]).withQuantity(Integer.parseInt(items[1])).withPriceItem(new BigDecimal(items[2]))
				.withTotalItem(getPriceItem().multiply(new BigDecimal(quantity)));

	}

	private boolean isValid(String[] itemData) {
		return itemData.length == 3;
	}

}